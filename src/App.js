import React, { Component } from "react";
import Lottery from "./Lottery";
import "./App.css";

class App extends Component {
  render() {
    return (
      <div className="App">
        <div>
          <Lottery />
          <Lottery title="Mini Daily" maxNum={10} numBalls={4} />
        </div>
      </div>
    );
  }
}

export default App;
